import * as readlineSync from 'readline-sync'
import { generateRandomCode } from '../global';

class Pessoa {
    private _nome: string;
    private _matricula: string;

    public get nome() {
        return this._nome
    }

    public set nome(nome: string) {
        this._nome = nome
    }

    public get matricula() {
        return this._matricula
    }

    public set matricula(matricula: string) {
        this._matricula = matricula
    }

    preencheDadosPadraoPessoa() {
        this.matricula = generateRandomCode()
        this.nome = readlineSync.question('Nome: ')
    }

}

export default Pessoa;