import * as readlineSync from 'readline-sync'

import repoPessoas from './repoPessoas'
import repoProdutos from '../Produtos/repoProdutos'
import repoLocacoes from '../Locacoes/repoLocacoes'

import Funcionario from "./Funcionario";

import { colors } from '../global'

import { DateTime } from 'luxon'
import Locacao from '../Locacoes/Locacao';

class Operador extends Funcionario {
    private _isOperador: boolean = true

    constructor() {
        super()
    }

    novaLocacao() {
        const matriculaCliente = readlineSync.question('Matricula do cliente: ')

        let cliente

        repoPessoas.forEach(c => {
            if (c.matricula === matriculaCliente) {
                cliente = c
                console.log(colors.successColor, `Cliente ${c.nome}`)
            }
        })

        if (!cliente) {
            console.log(colors.errorColor, 'Nenhum cliente encontrado com essa matrícula!')
            return
        }

        const codigoProduto = readlineSync.question('Código do produto: ')

        let produto

        repoProdutos.forEach(p => {
            if (p.codigo === codigoProduto) {
                produto = p
                console.log(colors.successColor, `Produto ${p.titulo}`)
            }
        })

        if (!produto) {
            console.log(colors.errorColor, 'Nenhum produto encontrado com esse código!')
            return
        }

        if (produto.locado) {
            console.log(colors.errorColor, 'Esse produto já está alugado!')
            return
        }

        const novaLocacao = new Locacao()
        novaLocacao.matriculaCliente = cliente.matricula
        novaLocacao.codigoProduto = produto.codigo
        novaLocacao.dataSaida = DateTime.local().toFormat('dd/LL/yyyy')
        novaLocacao.dataEntrega = DateTime.local().plus({ days: 7 }).toFormat('dd/LL/yyyy')

        repoLocacoes.set(novaLocacao.idLocacao, novaLocacao)

        produto.locado = true

        console.log(colors.successColor, `\nNova locação realizada no nome de ${cliente.nome}, produto: ${produto.titulo}.`)
        console.log(`ID de Locação: ` + colors.warningColor, `${novaLocacao.idLocacao}`)
        console.log(`Data de devolução: ${novaLocacao.dataEntrega}`)
    }

    excluirLocacao() {
        const idLocacao = readlineSync.question('Digite o código da locação a ser excluída: ')
        const locacao = repoLocacoes.get(idLocacao)

        if (locacao) {
            let produto = repoProdutos.get(locacao.codigoProduto)
            produto.locado = false
            repoLocacoes.delete(idLocacao)
            console.log(colors.successColor, 'Locação excluída com sucesso.')
        } else {
            console.log(colors.errorColor, 'Nenhuma locação encontrada com esse ID.')
        }
    }

    darBaixaLocacao() {
        const idLocacao = readlineSync.question('Digite o ID da locação: ')

        let locacao = repoLocacoes.get(idLocacao)

        if (locacao) {
            locacao = repoLocacoes.get(idLocacao)
            console.log(colors.successColor, `Locação localizada! Produto: ${repoProdutos.get(locacao.codigoProduto).titulo}`)
        } else {
            console.log(colors.errorColor, 'Nenhuma locação encontrada com esse ID.')
            return
        }

        do {
            var strDataDevolucao = readlineSync.question('Digite a data que o produto foi devolvido no formato dia/mes/ano\n')
            if (strDataDevolucao.match(/\d{2}\/\d{2}\/\d{4}/)) break;
            else console.log(colors.errorColor + 'Data inválida, digite novamente!')
        } while (true)

        const dataDevolucao = DateTime.fromFormat(strDataDevolucao, 'dd/LL/yyyy')
        const dataPrevistaDevolucao = DateTime.fromFormat(locacao.dataEntrega, 'dd/LL/yyyy')

        const { days } = dataDevolucao.diff(dataPrevistaDevolucao, 'days').toObject()

        const multa = days > 0 ? days : 0

        console.log(`Valor pela diaria: ${repoProdutos.get(locacao.codigoProduto).calcularDiaria() * days}`)
        console.log(`Valor de multa: ${repoProdutos.get(locacao.codigoProduto).calcularMulta(multa).toFixed(2)}`)

        const total = repoProdutos.get(locacao.codigoProduto).calcularDiaria() + repoProdutos.get(locacao.codigoProduto).calcularMulta(multa)
        console.log(colors.warningColor, `TOTAL A PAGAR: ${total.toFixed(2)}`)

        let baixa = readlineSync.keyInYNStrict('Dar baixa?')

        if (baixa) {
            repoLocacoes.delete(idLocacao)
            console.log(colors.successColor, 'Baixa dada com sucesso.')
        } else return
    }
}

export default Operador