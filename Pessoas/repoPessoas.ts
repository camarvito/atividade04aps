import { generateRandomCode } from '../global';
import Cliente from './Cliente';
import Gerente from './Gerente'
import Operador from './Operador';

let pessoaMap = new Map<string, any>();

const admin = new Gerente();

admin.matricula = generateRandomCode()
admin.nome = 'ADMINISTRADOR'
admin.login = 'admin'
admin.senha = 'admin'

const operador = new Operador();

operador.matricula = generateRandomCode()
operador.nome = 'OPERADOR'
operador.login = 'operador'
operador.senha = 'admin'

const cliente = new Cliente();
cliente.matricula = '00001'
cliente.nome = 'Zezinho'
cliente.sexo = 'Homem'
cliente.idade = 21
cliente.endereco = 'Rua do Sol'


pessoaMap.set(admin.matricula, admin);
pessoaMap.set(operador.matricula, operador);
pessoaMap.set(cliente.matricula, cliente);

export default pessoaMap