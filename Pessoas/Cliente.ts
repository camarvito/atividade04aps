import * as readlineSync from 'readline-sync'
import Pessoa from "./Pessoa";

class Cliente extends Pessoa {
    private _endereco: string;
    private _idade: number;
    private _sexo: string;
    private _isCliente: boolean = true;

    constructor() {
        super()
    }

    public get endereco() {
        return this._endereco
    }

    public set endereco(endereco: string) {
        this._endereco = endereco
    }

    public get idade() {
        return this._idade
    }

    public set idade(idade: number) {
        this._idade = idade
    }

    public get sexo() {
        return this._sexo
    }

    public set sexo(sexo: string) {
        this._sexo = sexo
    }

    public get isCliente() {
        return this._isCliente
    }

    preencheDadosPadraoCliente() {
        this.idade = readlineSync.questionInt('Idade do cliente: ')
        this.endereco = readlineSync.question('Endereço do cliente: ')

        const sexos = ['Homem', 'Mulher']
        const index = readlineSync.keyInSelect(sexos, 'Sexo do cliente: ')
        this.sexo = sexos[index]
    }
}

export default Cliente