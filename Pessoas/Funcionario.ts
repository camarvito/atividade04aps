import * as readlineSync from 'readline-sync'

import Pessoa from './Pessoa';

class Funcionario extends Pessoa {
    private _login: string;
    private _senha: string;

    public get login() {
        return this._login
    }

    public set login(login: string) {
        this._login = login
    }

    public get senha() {
        return this._senha
    }

    public set senha(senha: string) {
        this._senha = senha
    }

    preencheDadosPadraoFuncionario() {
        this.login = readlineSync.question('Digite o novo login do funcionário: ')
        this.senha = readlineSync.question('Digite a nova senha do funcionário: ')
    }
}


export default Funcionario;
