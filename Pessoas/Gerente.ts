import * as readlineSync from 'readline-sync'

import { colors, generateRandomCode } from '../global'

import Funcionario from './Funcionario';

import BluRay from '../Produtos/Blu-ray'
import DVD from '../Produtos/DVD'
import VHS from '../Produtos/VHS'
import CD from '../Produtos/CD'
import LP from '../Produtos/LP'

import repoProdutos from '../Produtos/repoProdutos'
import repoPessoas from '../Pessoas/repoPessoas'

import Filme from '../Produtos/Filme';
import Cliente from './Cliente';
import Operador from './Operador';

class Gerente extends Funcionario {
    optIndex: number;

    constructor() {
        super();
    }

    adicionarOperador() {
        const novoOperador = new Operador()
        novoOperador.preencheDadosPadraoPessoa()
        novoOperador.preencheDadosPadraoFuncionario()
        repoPessoas.set(novoOperador.matricula, novoOperador)
        console.log(colors.successColor, `Novo operador ${novoOperador.nome} cadastrado com sucesso.`)
    }

    adicionarCliente() {
        const novoCliente = new Cliente()
        novoCliente.preencheDadosPadraoPessoa()
        novoCliente.preencheDadosPadraoCliente()
        repoPessoas.set(novoCliente.matricula, novoCliente)
        console.log(colors.successColor, `Novo cliente ${novoCliente.nome} cadastrado com sucesso. Matricula: ${novoCliente.matricula}`)
    }

    adicionarProduto() {
        do {
            const opcoesProdutos = ['Blu-ray', 'DVD', 'VHS', 'CD', 'LP']
            this.optIndex = readlineSync.keyInSelect(opcoesProdutos, `Qual produto deseja adicionar?`, { cancel: 'Sair' })

            switch (this.optIndex) {
                case 0:
                    const novoBluRay = new BluRay()

                    novoBluRay.preencheDadosPadrao()

                    const idiomas = []
                    const idioma = readlineSync.question('Qual o idioma do filme? ')
                    idiomas.push(idioma)

                    console.log('Se o filme possui mais algum idioma, digite-o e pressione enter, caso contrário aperte enter com o input vazio.')

                    do {
                        const idioma = readlineSync.question('Idioma: ')
                        if (!idioma) break;
                        idiomas.push(idioma)
                    } while (true)

                    novoBluRay.idiomas = idiomas

                    repoProdutos.set(novoBluRay.codigo, novoBluRay)
                    console.log(colors.successColor, `\nProduto ${novoBluRay.titulo} cadastrado com sucesso! Código: ${novoBluRay.codigo}`)
                    break;

                case 1:
                    const newDVD = new DVD()

                    newDVD.preencheDadosPadrao()
                    repoProdutos.set(newDVD.codigo, newDVD)

                    console.log(colors.successColor, `\nProduto ${newDVD.titulo} cadastrado com sucesso! Código: ${newDVD.codigo}`)
                    break;

                case 2:
                    const newVHS = new VHS()

                    newVHS.preencheDadosPadrao()
                    repoProdutos.set(newVHS.codigo, newVHS)

                    console.log(colors.successColor, `\nProduto ${newVHS.titulo} cadastrado com sucesso! Código: ${newVHS.codigo}`)
                    break;

                case 3:
                    const newCD = new CD()

                    newCD.preencheDadosPadrao()

                    const isDuplo = readlineSync.keyInYNStrict('O CD é o duplo? ')
                    newCD.duplo = isDuplo

                    repoProdutos.set(newCD.codigo, newCD)

                    console.log(colors.successColor, `\nProduto ${newCD.titulo} cadastrado com sucesso! Código: ${newCD.codigo}`)
                    break;

                case 5:
                    const newLP = new LP()

                    newLP.preencheDadosPadrao()

                    const isRaro = readlineSync.keyInYNStrict('O LP é raro? ')
                    newLP.raro = isRaro

                    repoProdutos.set(newLP.codigo, newLP)

                    console.log(colors.successColor, `\nProduto ${newLP.titulo} cadastrado com sucesso! Código: ${newLP.codigo}`)
                    break;

                default:
                    break;
            }

        } while (this.optIndex >= 0)
    }




}


export default Gerente;
