# Simulação - Sistema de uma Locadora

O sistema foi construido em typescript, utilizando padrões OO.

# Requisitos

Com o Node instalado na sua máquina, instale globalmente os seguintes pacotes para rodar os arquivos .ts:

```
npm install -g typescript
npm install -g ts-node
```

Em seguida na pasta raiz, rode o seguinte comando para instalar as dependências do projeto:

```
npm i
```

# Como executar

Na pasta raiz do projeto você pode rodar o comando:

```
npm start
```

Ou executar o arquivo main.ts com:

```
ts-node main.ts
```

## Usuários padrão

*Gerente*

Login: admin  
Senha: admin

*Operador*

Login: operador  
Senha: admin


Qualquer dúvida na correção entre em contato comigo.  

**Victor Camargo**  
**vcso16@gmail.com**

