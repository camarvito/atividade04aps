import * as readlineSync from 'readline-sync'

import { colors } from './global'

import repoPessoas from './Pessoas/repoPessoas'
import repoProdutos from './Produtos/repoProdutos'

let opcoesPerfil = ['Gerente', 'Operador de Sistema']

let isLogado, user

let i, j, k

do {
    i = readlineSync.keyInSelect(opcoesPerfil, 'Bem-vindo, escolha um perfil:', { cancel: 'Sair' })

    if (i < 0) break;

    let userLogin = readlineSync.question('\nLogin: ')
    let userPassword = readlineSync.question('Senha: ', { hideEchoBack: true })

    repoPessoas.forEach(p => {
        if (p.login === userLogin && userPassword === p.senha) {
            isLogado = true
            user = p
            console.log(colors.successColor, '\nLogado com sucesso.')
        }
    })

    if (!isLogado) console.log(colors.errorColor, 'Login e/ou senha incorretos! Tente novamente.')

    if (isLogado) {
        if (i === 0) {
            do {
                let opcoesGerente = [
                    'Cadastrar Produto', 'Cadastrar Cliente', 'Cadastrar Operador',
                    'Listar Produtos', 'Listar Clientes', 'Listar Operadores',
                    'Procurar Produto', 'Procurar Cliente', 'Procurar Operador']

                j = readlineSync.keyInSelect(opcoesGerente, `Olá Gerente ${user.nome}`, { cancel: 'Sair' })

                switch (j) {
                    case 0:
                        user.adicionarProduto()
                        break;

                    case 1:
                        user.adicionarCliente()
                        break;

                    case 2:
                        user.adicionarOperador()
                        break;

                    case 3:
                        repoProdutos.forEach(p => console.log(p))
                        break;

                    case 4:
                        repoPessoas.forEach(c => {
                            if (c.isCliente)
                                console.log(c)
                        })
                        break;

                    case 5:
                        repoPessoas.forEach(o => {
                            if (o._isOperador)
                                console.log(o)
                        })
                        break;

                    case 6:
                        let codigoProduto = readlineSync.question('Digite o código do produto: ')
                        let produto = repoProdutos.get(codigoProduto)

                        if (produto) {
                            console.log(produto)
                        } else {
                            console.log(colors.errorColor, 'Nenhum produto encontrado com esse código!')
                        }

                        break;

                    case 7:
                        let codigoCliente = readlineSync.question('Digite o código do cliente: ')
                        let cliente = repoPessoas.get(codigoCliente)

                        if (cliente) {
                            console.log(cliente)
                        } else {
                            console.log(colors.errorColor, 'Nenhum cliente encontrado com esse código!')
                        }

                        break;

                    case 8:
                        let codigoOperador = readlineSync.question('Digite o código do operador: ')
                        let operador = repoPessoas.get(codigoOperador)

                        if (operador && operador.isOperador) {
                            console.log(operador)
                        } else {
                            console.log(colors.errorColor, 'Nenhum operador encontrado com esse código!')
                        }
                        break;

                    default:
                        break;
                }

            } while (j >= 0)

            isLogado = false

        } else {

            do {
                let opcoesOperador = [
                    'Fazer locação', 'Dar baixa em locação', 'Excluir locação',
                    'Procurar Produto', 'Procurar Cliente'
                ]

                let k = readlineSync.keyInSelect(opcoesOperador, `Olá Operador ${user.nome}`, { cancel: 'Sair' })

                switch (k) {
                    case 0:
                        user.novaLocacao()
                        break;

                    case 1:
                        user.darBaixaLocacao()
                        break;

                    case 2:
                        user.excluirLocacao()
                        break;

                    case 3:
                        let codigoProduto = readlineSync.question('Digite o código do produto: ')
                        let produto = repoProdutos.get(codigoProduto)

                        if (produto) {
                            console.log(produto)
                        } else {
                            console.log(colors.errorColor, 'Nenhum produto encontrado com esse código!')
                        }

                        break;

                    case 4:
                        let codigoCliente = readlineSync.question('Digite o código do cliente: ')
                        let cliente = repoPessoas.get(codigoCliente)

                        if (cliente) {
                            console.log(cliente)
                        } else {
                            console.log(colors.errorColor, 'Nenhum cliente encontrado com esse código!')
                        }
                        break;

                    default:
                        break;
                }
            } while (k >= 0)

            isLogado = false
        }
    }

} while (i >= 0)

console.log('Obrigado!')