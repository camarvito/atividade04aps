export const colors = {
    errorColor: '\x1b[31m%s\x1b[0m',
    successColor: '\x1b[34m%s\x1b[0m',
    warningColor: '\x1b[33m%s\x1b[0m'
}

export function generateRandomCode() {
    let randomFixedInteger = function (length = 5) {
        return Math.floor(Math.pow(10, length - 1) + Math.random() * (Math.pow(10, length) - Math.pow(10, length - 1) - 1));
    }

    return randomFixedInteger().toString()
}
