import DVD from './DVD';
import Produto from './Produto'

let produtosMap = new Map<string, Produto>();

const filme = new DVD();
filme.codigo = '00001'
filme.titulo = 'Vingadores'
filme.genero = 'Ação'
filme.anoLancamento = 2012
filme.arranhado = false
filme.duracao = 200
filme.locado = false

produtosMap.set(filme.codigo, filme)

export default produtosMap