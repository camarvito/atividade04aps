import { generateRandomCode } from '../global';
import Filme from './Filme';

class VHS extends Filme {
    cores: boolean = false;

    constructor() {
        super();
        this.codigo = generateRandomCode()
    }
}

export default VHS