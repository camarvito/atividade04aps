import { generateRandomCode } from '../global';
import Filme from './Filme';

class DVD extends Filme {
    arranhado: boolean = false;

    constructor() {
        super();
        this.codigo = generateRandomCode()
    }

}

export default DVD