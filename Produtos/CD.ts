import { generateRandomCode } from '../global'
import Musica from './Musica'

class CD extends Musica {
    arranhado: boolean = false
    duplo: boolean

    constructor() {
        super()
        this.codigo = generateRandomCode()
    }
}

export default CD