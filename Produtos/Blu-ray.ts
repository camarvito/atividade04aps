import { generateRandomCode } from '../global';
import Filme from './Filme';

class BluRay extends Filme {
    idiomas: string[];

    constructor() {
        super();
        this.codigo = generateRandomCode()
    }
}

export default BluRay