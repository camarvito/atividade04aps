class Produto {
    codigo: string;
    titulo: string;
    genero: string;
    locado: boolean = false;

    calcularDiaria() {
        return this.codigo.charCodeAt(0) / 10
    }

    calcularMulta(diasAtraso: number) {
        return this.calcularDiaria() * 1.5 * diasAtraso
    }
}

export default Produto;