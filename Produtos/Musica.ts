import * as readlineSync from 'readline-sync'

import Produto from "./Produto";

class Musica extends Produto {
    compositor: string;
    numFaixas: number;

    preencheDadosPadrao() {
        this.titulo = readlineSync.question('Qual o nome do álbum? ')
        this.genero = readlineSync.question('Qual o gênero do álbum? ')
        this.compositor = readlineSync.question('Quem é o compositor da música? ')
        this.numFaixas = parseInt(readlineSync.question('Qual o número de faixas do álbum? '))
    }
}

export default Musica;