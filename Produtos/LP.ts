import { generateRandomCode } from '../global';
import Musica from './Musica';

class LP extends Musica {
    raro: boolean

    constructor() {
        super()
        this.codigo = generateRandomCode()
    }
}

export default LP