import * as readlineSync from 'readline-sync'

import Produto from "./Produto";

class Filme extends Produto {
    anoLancamento: number;
    duracao: number;

    preencheDadosPadrao() {
        this.titulo = readlineSync.question('Qual o nome do filme? ')
        this.genero = readlineSync.question('Qual o gênero do filme? ')
        this.anoLancamento = parseInt(readlineSync.question('Qual o ano de seu lançamento? '))
        this.duracao = parseInt(readlineSync.question('Qual a duração (em minutos) do filme? '))
    }

}



export default Filme;