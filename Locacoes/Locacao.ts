import { generateRandomCode } from "../global"

class Locacao {
    private _idLocacao: string
    private _codigoProduto: string
    private _matriculaCliente: string
    private _dataSaida: string
    private _dataEntrega: string

    constructor() {
        this.idLocacao = generateRandomCode()
    }

    public get idLocacao() {
        return this._idLocacao
    }

    public set idLocacao(idLocacao: string) {
        this._idLocacao = idLocacao
    }

    public get codigoProduto() {
        return this._codigoProduto
    }

    public set codigoProduto(codigoProduto: string) {
        this._codigoProduto = codigoProduto
    }

    public get matriculaCliente() {
        return this._matriculaCliente
    }

    public set matriculaCliente(matriculaCliente: string) {
        this._matriculaCliente = matriculaCliente
    }

    public get dataSaida() {
        return this._dataSaida
    }

    public set dataSaida(dataSaida: string) {
        this._dataSaida = dataSaida
    }

    public get dataEntrega() {
        return this._dataEntrega
    }

    public set dataEntrega(dataEntrega: string) {
        this._dataEntrega = dataEntrega
    }

}

export default Locacao